# Rendu Atelier Persistance de donnée

lien vers le [backend](https://gitlab.com/Arnaudboy/atelierpersistancedesdonnees).
Le backend a été développé avec java SDK 18, maven et spring boot. 
Je n'ai aucune idée de ce qu'il est nécéssaire de faire pour l'installer sur une autre machine (ou plutôt je ne saurai
pas donner d'instruction précise)

## Installation

Un docker compose est à votre disposition. Les ports 3000 (react), 3306 (bdd), 8080 (phpMyAdmin), 8081 (back) doivent être libre.

puis lancer `docker compose up`

### supression d'un livre

Un livre subit une supression logique. Un boolean est mis à jour en lieux et place d'une suppresion

### supression catégorie

Une catégorie est vue comme un rayon pour placer le livre, ainsi les livres ne possèdent qu'une seule catégorie.
Lors de la suppression l'ID est remplacé par l'ID de la catégorie `RESERVE`
Cette categorie doit être présente en BDD pensez à utilser le seed une fois que le back est lancé
