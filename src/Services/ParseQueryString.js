export const parseQueryString = (queryString) => {
    const object = {}
    if (queryString[0] === "?") queryString = queryString.slice(1)

    const queryArray = queryString.split("&")
    for (const query of queryArray) {
        const queryArray = query.split('=')
        object[queryArray[0]] = decodeURI(queryArray[1])
    }
    return object
}