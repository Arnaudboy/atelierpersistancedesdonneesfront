import {createContext, useContext, useEffect, useState} from 'react';

 const CategorieContext = createContext({});

 const CategorieProvider = (props) => {
    const [categorieDatas, setCategorieDatas] = useState([])
    const [loadingCategories, setLoadingCategories] = useState(true)

    useEffect(() => {
        fetch("http://localhost:8081/categorie")
            .then(res => res.json())
            .then(json => setCategorieDatas(json))
            .then(setLoadingCategories(false))
        // eslint-disable-next-line
    }, [])

    return (
        <CategorieContext.Provider
            value={{
                loadingCategories,
                categorieDatas,
            }}
            {...props}
        />
    )
}

 const useCategories = () => useContext(CategorieContext);

export { CategorieProvider, useCategories };