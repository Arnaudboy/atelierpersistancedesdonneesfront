
const AddBook = () => {

    const handleSubmit = (event) => {
        event.preventDefault()
        const body = {
            "titre" : event.target.titre.value,
            "dateDeParution" : event.target.dateDeParution.value,
            "nombreDePages" : event.target.nombreDePages.value,
        }
        fetch("http://localhost:8081/livre", {
            method: "POST",
            mode: "cors",
            headers : {
                'Content-type' : 'application/json',
                Accept : 'application/json'},
            body: JSON.stringify(body)
        }).then(res => {
            if (res.status === 204){
                // eslint-disable-next-line no-restricted-globals
                return window.location.assign("/livre")
            }
        })
    }

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="titre"> TITRE DU LIVRE </label>
            <input id="titre"/>
            <label htmlFor="dateDeParution"> Date de parution </label>
            <input id="dateDeParution"/>
            <label htmlFor="nombreDePages"> Nombre de pages </label>
            <input id="nombreDePages"/>
            <input type="submit" value="Submit" />
        </form>
    )

}

export default AddBook