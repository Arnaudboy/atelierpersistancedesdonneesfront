import {useLocation} from "react-router-dom";
import {useEffect, useState} from "react";
import {parseQueryString} from "../Services/ParseQueryString";

const Emprunter = () => {
    const [livreId, setLivreId] = useState("")
    const [adherentList, setAdherentList] = useState([])
    const [loading, setLoading] = useState(true)
    const {search} = useLocation()

    useEffect(() => {
        const queryStringParsed = parseQueryString(search)
        setLivreId(queryStringParsed.livreId)
        fetch("http://localhost:8081/adherent")
            .then(res => res.json())
            .then(data => setAdherentList(data))
            .then(setLoading(false))
        // eslint-disable-next-line
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault()
        const body = {
            "idLivre" : livreId,
            "idAdherent" : event.target.adherent.value
        }
        fetch("http://localhost:8081/emprunt", {
            method: "POST",
            mode: "cors",
            headers : {
                'Content-type' : 'application/json',
                Accept : 'application/json'},
            body: JSON.stringify(body)
        }).then(res => {
            if (res.status === 200){
                // eslint-disable-next-line no-restricted-globals
                return window.location.assign("/livre")
            }
        })
    }

    return (
        <form onSubmit={handleSubmit}>
            {loading ? <h1>LOADING</h1> :
                <select name="adherent" id="adherent">
                    {adherentList.map(adherent => (
                        <option value={adherent.id} key={adherent.id}>{adherent.email}</option>
                    ))}
                </select>}
            <input type="submit" value="Submit" />
        </form>
    )
}
export default Emprunter