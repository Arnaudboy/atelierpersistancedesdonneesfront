const AddCategorie = () => {
    const handleSubmit = (event) => {
        event.preventDefault()
        const body = {
            "nom" : event.target.nom.value,
        }
        fetch("http://localhost:8081/categorie", {
            method: "POST",
            mode: "cors",
            headers : {
                'Content-type' : 'application/json',
                Accept : 'application/json'},
            body: JSON.stringify(body)
        }).then(res => {
            if (res.status === 204){
                // eslint-disable-next-line no-restricted-globals
                return window.location.assign("/categorie")
            }
        })
    }

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="nom"> Nom de la Catégorie </label>
            <input id="nom"/>
            <input type="submit" value="Submit" />
        </form>
    )
}

export default AddCategorie