const AddAdherent = () => {
    const handleSubmit = (event) => {
        event.preventDefault()
        const body = {
            "email" : event.target.email.value,
            "nom" : event.target.nom.value,
            "prenom" : event.target.prenom.value,
        }
        fetch("http://localhost:8081/adherent", {
            method: "POST",
            mode: "cors",
            headers : {
                'Content-type' : 'application/json',
                Accept : 'application/json'},
            body: JSON.stringify(body)
        }).then(res => {
            if (res.status === 204){
                // eslint-disable-next-line no-restricted-globals
                return window.location.assign("/adherent")
            }
        })
    }

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="email"> EMAIL </label>
            <input id="email"/>
            <label htmlFor="prenom"> Prénom </label>
            <input id="prenom"/>
            <label htmlFor="nom"> NOM </label>
            <input id="nom"/>
            <input type="submit" value="Submit" />
        </form>
    )
}

export default AddAdherent