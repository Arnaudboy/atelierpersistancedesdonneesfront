import React from "react";
import Button from "react-bootstrap/Button";
import {Outlet} from "react-router-dom";
import {useCategories} from "../Context/CategorieContext";
import "../css/GetAllCategories.css"
const GetAllCategorie = () => {
const {loadingCategories, categorieDatas} = useCategories()
    const handleDelete = (event, id) => {
        fetch(`http://localhost:8081/categorie?id=${id}`, {method: "DELETE"})
            .then(res => {
                if (res.status === 204) window.location.reload()
            })
    }

    return (
        <div className="get-all-categories">
            {loadingCategories ? <h1>LOADING</h1> :
                <div>
                    <table>
                        <thead>
                        <tr>
                            <th colSpan="3">Toutes les catégories</th>
                        </tr>
                        <tr>
                            <th>Nom</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            categorieDatas.map((data, index) => (
                                <tr key={index}>
                                    <td>{data.nom}</td>
                                    <td>
                                        {<Button
                                            href={`/categorie/update?nom=${data.nom}&id=${data.id}`}> Update </Button>}
                                        {data.nom !== "RESERVE" && <Button onClick={e => handleDelete(e, data.id)}> Delete </Button>}
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                    <Button href={"/categorie/create"}>Ajouter une catégorie</Button>
                </div>}
            <div id="update">
                <Outlet />
            </div>
        </div>)
}
export default GetAllCategorie