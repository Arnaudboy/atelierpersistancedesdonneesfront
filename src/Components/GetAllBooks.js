import React, {useEffect, useState} from "react";
import Button from 'react-bootstrap/Button';
import {Outlet} from "react-router-dom";
import "../css/GetAllBooks.css"
const GetAllBooks = () => {

    const [datas, setDatas] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        fetch("http://localhost:8081/livre")
            .then(res => res.json())
            .then(json => setDatas(json))
            .then( setLoading(false))
        // eslint-disable-next-line
        }, [])

    const handleDelete = (event, id) => {
        fetch(`http://localhost:8081/livre?id=${id}`, {method: "DELETE"})
            .then(res => {
                if (res.status === 204) window.location.reload()
            })
    }

    const handleReturn = (event, id) => {
        fetch(`http://localhost:8081/emprunt?id=${id}`, {method: "DELETE"})
            .then(res => {
                if (res.status === 200) window.location.reload()
            })
    }

    return (
        <div className="get-all-books">
            {loading ? <h1>LOADING</h1> :
                <div>
                    <table>
                        <thead>
                        <tr>
                            <th colSpan="3">Tous les livres</th>
                        </tr>
                        <tr>
                            <th>Titre</th>
                            <th>Nombre de pages</th>
                            <th>Année de parution</th>
                            <th>Catégorie</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            datas.map((data, index) => (
                                <tr key={index}>
                                    <td>{data.titre}</td>
                                    <td>{data.nombreDePages}</td>
                                    <td>{data.dateDeParution}</td>
                                    <td>{data.categorieNom}</td>
                                    {!data.isDeleted && data.isAvailable &&
                                        <td>
                                            {<Button
                                                href={`/livre/update?titre=${data.titre}&nombreDePages=${data.nombreDePages}&dateDeParution=${data.dateDeParution}&nomCategorie=${data.categorieNom}&id=${data.id}`}> Update </Button>}
                                            {<Button onClick={e => handleDelete(e, data.id)}> Delete </Button>}
                                            {<Button href={`/livre/emprunter?livreId=${data.id}`}>Emprunter</Button>}
                                        </td>
                                    }
                                    {data.isDeleted &&
                                        <td>
                                            Le livre a été supprimé
                                        </td>
                                    }
                                    {!data.isAvailable &&
                                        <td>
                                            Le livre a été emprunté
                                            <Button onClick={e => handleReturn(e, data.id)}>Retour du livre</Button>
                                        </td>
                                    }
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                    <Button href={"/livre/create"}>Ajouter un livre</Button>
                    <Button href={"/categorie/create"}>Créer une catégorie</Button>
                </div>}
            <div id="update">
                <Outlet />
            </div>
    </div>)
}

export default GetAllBooks