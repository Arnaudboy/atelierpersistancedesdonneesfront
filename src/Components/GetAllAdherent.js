import React, {useEffect, useState} from "react";
import Button from 'react-bootstrap/Button';
import {Outlet} from "react-router-dom";
import "../css/GetAllAdherents.css"
const GetAllAdherent = () => {

    const [datas, setDatas] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        fetch("http://localhost:8081/adherent")
            .then(res => res.json())
            .then(json => setDatas(json))
            .then(setLoading(false))
        // eslint-disable-next-line
        }, [])

    return (
        <div className="get-all-books">
            {loading ? <h1>LOADING</h1> :
                <div>
                    <table>
                        <thead>
                        <tr>
                            <th colSpan="3">Tous les adhérents</th>
                        </tr>
                        <tr>
                            <th>email</th>
                            <th>NOM</th>
                            <th>Prénom</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            datas.map((data, index) => (
                                <tr key={index}>
                                    <td>{data.email}</td>
                                    <td>{data.nom}</td>
                                    <td>{data.prenom}</td>
                                    <td>
                                        {<Button
                                            href={`/adherent/update?email=${data.email}&nom=${data.nom}&prenom=${data.prenom}&id=${data.id}`}> Update </Button>}
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                    <Button href={"/adherent/create"}>Ajouter un adherent</Button>
                </div>}
            <div id="update">
                <Outlet />
            </div>
    </div>)
}

export default GetAllAdherent