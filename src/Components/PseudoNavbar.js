import "../css/PseudoNavbar.css"
const PseudoNavbar = () => {

    return (
        <div className="pseudo-navbar">
            <a href="/livre">Livre</a>
            <a href="/adherent">Adherent</a>
            <a href="/categorie">Categorie</a>
        </div>
    )
}
export default PseudoNavbar