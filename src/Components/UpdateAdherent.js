import {useEffect, useState} from "react";
import {parseQueryString} from "../Services/ParseQueryString";
import {useLocation} from "react-router-dom";

const UpdateAdherent = () => {
    const [email, setEmail] = useState("")
    const [nom, setNom] = useState("")
    const [prenom, setPrenom] = useState("")
    const [id, setId] = useState("")
    const [loading, setLoading] = useState(true)
    const {search} = useLocation()

    useEffect(() => {
        const queryStringParsed = parseQueryString(search)
        setEmail(queryStringParsed.email)
        setNom(queryStringParsed.nom)
        setPrenom(queryStringParsed.prenom)
        setId(queryStringParsed.id)
        setLoading(false);
        // eslint-disable-next-line
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault()
        const body = {
            "id": id,
            "email" : email,
            "nom" : nom,
            "prenom" : prenom,
        }
        fetch("http://localhost:8081/adherent", {
            method: "PUT",
            mode: "cors",
            headers : {
                'Content-type' : 'application/json',
                Accept : 'application/json'},
            body: JSON.stringify(body)
        }).then(res => {
            if (res.status === 204){
                // eslint-disable-next-line no-restricted-globals
                return window.location.assign("/adherent")
            }
        })
    }

    return (
        <div>
            {loading ? (<h1>LOADING</h1>) :
                <form onSubmit={handleSubmit}>
                    <label htmlFor="titre"> Email </label>
                    {email !== "" &&
                        <input id="titre" value={email} name="email" onChange={e => setEmail(e.target.value)}/>}
                    <label htmlFor="dateDeParution"> Prénom </label>
                    {prenom !== "" && <input id="dateDeParution" value={prenom} name="prenom"
                                             onChange={e => setPrenom(e.target.value)}/>}
                    <label htmlFor="nombreDePages"> NOM </label>
                    {nom !== "" &&
                        <input id="nombreDePages" value={nom} name="nom" onChange={e => setNom(e.target.value)}/>}
                    <input type="submit" value="Submit"/>
                </form>}
        </div>
    )
}

export default UpdateAdherent