import {useLocation} from "react-router-dom";
import {useEffect, useState} from "react";
import {parseQueryString} from "../Services/ParseQueryString";
import {useCategories} from "../Context/CategorieContext";

const UpdateBook = () => {
    const [titre, setTitre] = useState("")
    const [dateDeParution, setDateDeParution] = useState("")
    const [nombreDePages, setNombreDePages] = useState("")
    const [nomCategorie, setNomCategorie] = useState("")
    const [loading, setLoading] = useState(true)
    const [id, setId] = useState("")
    const {search} = useLocation()
    const {categorieDatas, loadingCategories} = useCategories()

    useEffect(() => {
        const queryStringParsed = parseQueryString(search)
        setTitre(queryStringParsed.titre)
        setDateDeParution(queryStringParsed.dateDeParution)
        setNombreDePages(queryStringParsed.nombreDePages)
        setId(queryStringParsed.id)
        setNomCategorie(queryStringParsed.nomCategorie)
        setLoading(false)
        // eslint-disable-next-line
    }, [])
    const handleSubmit = (event) => {
        event.preventDefault()
        const body = {
            "id": id,
            "titre" : titre,
            "dateDeParution" : dateDeParution,
            "nombreDePages" : nombreDePages,
            "idCategorie" : event.target.categorie.value
        }
        fetch("http://localhost:8081/livre", {
            method: "PUT",
            mode: "cors",
            headers : {
                'Content-type' : 'application/json',
                Accept : 'application/json'},
            body: JSON.stringify(body)
        }).then(res => {
            if (res.status === 204){
                // eslint-disable-next-line no-restricted-globals
                return window.location.assign("/livre")
            }
        })
    }

    return (
        <div>
            {loading ? (<h1>LOADING</h1>) :
                <form onSubmit={handleSubmit}>
                    <label htmlFor="titre"> TITRE DU LIVRE </label>
                    {titre !== "" &&
                        <input id="titre" value={titre} name="titre" onChange={e => setTitre(e.target.value)}/>}
                    <label htmlFor="dateDeParution"> Date de parution </label>
                    {dateDeParution !== "" && <input id="dateDeParution" value={dateDeParution} name="dateDeParution"
                                                     onChange={e => setDateDeParution(e.target.value)}/>}
                    <label htmlFor="nombreDePages"> Nombre de pages </label>
                    {nombreDePages !== "" && <input id="nombreDePages" value={nombreDePages} name="nombreDePages"
                                                    onChange={e => setNombreDePages(e.target.value)}/>}
                    <select name="categorie" id="categorie" defaultValue={nomCategorie}>
                        {!loadingCategories && categorieDatas.map(categorie => (
                            <option value={categorie.id} key={categorie.id}>{categorie.nom}</option>
                        ))}
                    </select>
                    <input type="submit" value="Submit"/>
                </form>}
        </div>
    )
}

export default UpdateBook