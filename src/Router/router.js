import {createBrowserRouter, Navigate} from "react-router-dom";
import AddBook from "../Components/AddBook";
import GetAllBooks from "../Components/GetAllBooks";
import UpdateBook from "../Components/UpdateBook";
import GetAllAdherent from "../Components/GetAllAdherent";
import UpdateAdherent from "../Components/UpdateAdherent";
import AddAdherent from "../Components/AddAdherent";
import Emprunter from "../Components/Emprunter";
import AddCategorie from "../Components/AddCategorie";
import GetAllCategorie from "../Components/GetAllCategorie";
import UpdateCategorie from "../Components/UpdateCategorie";


const router = createBrowserRouter([
    {
        path: "/",
        element: <Navigate to="/livre" replace />,
    },
    {
        path: "/livre",
        element: <GetAllBooks />,
        children: [
            {
                path: "update",
                element: <UpdateBook />
            },
            {
                path: "create",
                element: <AddBook />,
            },
            {
                path: "emprunter",
                element: <Emprunter />,
            },
        ]
    },
    {
        path: "/adherent",
        element: <GetAllAdherent/>,
        children : [
            {
                path: "update",
                element: <UpdateAdherent />
            },
            {
                path: "create",
                element: <AddAdherent />,
            },
        ]
    },
    {
        path: "/categorie",
        element: <GetAllCategorie/>,
        children : [
            {
                path: "update",
                element: <UpdateCategorie />
            },
            {
                path: "create",
                element: <AddCategorie />,
            },
        ]
    }
]);

export default router